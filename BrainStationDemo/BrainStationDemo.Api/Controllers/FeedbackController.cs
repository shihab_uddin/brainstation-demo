﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BrainStationDemo.Model.AppModels;
using BrainStationDemo.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BrainStationDemo.Api.Controllers
{
    [Route("api/feedback")]
    [ApiController]
    public class FeedbackController : ControllerBase
    {
        private readonly IFeedbackService _feedbackService;
        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }
        [HttpGet()]
        public ActionResult<IEnumerable<AppPost>> GetAll()
        {
            var data = _feedbackService.GetAll();
            if (data.Any())
                return Ok(data);
            return NotFound();
        }
    }
}
