﻿using System;
using System.Collections.Generic;

namespace BrainStationDemo.DAL.Models
{
    public partial class Comment
    {
        public Comment()
        {
            ReactionOnComment = new HashSet<ReactionOnComment>();
        }

        public long Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }
        public long PostId { get; set; }
        public long UserId { get; set; }

        public virtual Post Post { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ReactionOnComment> ReactionOnComment { get; set; }
    }
}
