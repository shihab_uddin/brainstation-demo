﻿using System;
using System.Collections.Generic;

namespace BrainStationDemo.DAL.Models
{
    public partial class ReactionOnComment
    {
        public long Id { get; set; }
        public bool IsLike { get; set; }
        public long UserId { get; set; }
        public long? CommentId { get; set; }

        public virtual Comment Comment { get; set; }
        public virtual User User { get; set; }
    }
}
