﻿using System;
using System.Collections.Generic;

namespace BrainStationDemo.DAL.Models
{
    public partial class User
    {
        public User()
        {
            Comment = new HashSet<Comment>();
            Post = new HashSet<Post>();
            ReactionOnComment = new HashSet<ReactionOnComment>();
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public int UserRole { get; set; }

        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Post> Post { get; set; }
        public virtual ICollection<ReactionOnComment> ReactionOnComment { get; set; }
    }
}
