﻿using BrainStationDemo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.DAL.Repository.Interfaces
{
    public interface IFeedbackRepository
    {
        List<Post> GetAllPost();
        List<Comment> GetAllComment();
        List<User> GetAllUser();
        List<ReactionOnComment> GetAllReaction();
    }
}
