﻿using BrainStationDemo.DAL.Models;
using BrainStationDemo.DAL.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrainStationDemo.DAL.Repository.Repositories
{
    public class FeedbackRepository:IFeedbackRepository
    {
        private readonly DbContextOptionsBuilder<BrainStationDemoContext> _dbContextOptionBuilder;
        public FeedbackRepository(string connectionString)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<BrainStationDemoContext>();
            _dbContextOptionBuilder.UseSqlServer(connectionString);
        }

        public List<Comment> GetAllComment()
        {
            using (var context = new BrainStationDemoContext(_dbContextOptionBuilder.Options))
            {
                return context.Comment.ToList();
            }
        }

        public List<Post> GetAllPost()
        {
            using (var context = new BrainStationDemoContext(_dbContextOptionBuilder.Options))
            {
                return context.Post.ToList();
            }
        }

        public List<ReactionOnComment> GetAllReaction()
        {
            using (var context = new BrainStationDemoContext(_dbContextOptionBuilder.Options))
            {
                return context.ReactionOnComment.ToList();
            }
        }

        public List<User> GetAllUser()
        {
            using (var context = new BrainStationDemoContext(_dbContextOptionBuilder.Options))
            {
                return context.User.ToList();
            }
        }
    }
}
