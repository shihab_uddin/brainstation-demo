﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Model.AppModels
{
    public class AppComment
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }
        public long PostId { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int TotalLike { get; set; }
        public int TotalDislike { get; set; }
        //public List<AppReactionOnComment> ReactionOnComments { get; set; }
    }
}
