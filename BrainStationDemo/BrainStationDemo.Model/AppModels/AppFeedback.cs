﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Model.AppModels
{
    public class AppFeedback
    {
        public long PostId { get; set; }
        public string PostedBy { get; set; }
        public int MyProperty { get; set; }
    }
}
