﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Model.AppModels
{
    public class AppPost
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int TotalComment { get; set; }
        public List<AppComment> Comments { get; set; }
    }
}
