﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Model.AppModels
{
    public class AppReactionOnComment
    {
        public long Id { get; set; }
        public bool IsLike { get; set; }
        public long UserId { get; set; }
        public long CommentId { get; set; }
    }
}
