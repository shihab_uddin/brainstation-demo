﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Model.AppModels
{
    public class AppUser
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public int UserRole { get; set; }
    }
}
