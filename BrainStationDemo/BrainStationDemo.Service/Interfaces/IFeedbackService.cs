﻿using BrainStationDemo.Model.AppModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BrainStationDemo.Service.Interfaces
{
    public interface IFeedbackService
    {
        List<AppPost> GetAll();
    }
}
