﻿using AutoMapper;
using BrainStationDemo.DAL.Repository.Interfaces;
using BrainStationDemo.DAL.Repository.Repositories;
using BrainStationDemo.Model.AppModels;
using BrainStationDemo.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrainStationDemo.Service.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly MapperConfiguration _mapperConfig;
        private readonly IFeedbackRepository _feedbackRepository;
        public FeedbackService(string connectionString)
        {
            _feedbackRepository = new FeedbackRepository(connectionString);
        }

        public List<AppPost> GetAll()
        {
            var postList = new List<AppPost>();
            var posts = _feedbackRepository.GetAllPost();
            var comments = _feedbackRepository.GetAllComment();
            var users = _feedbackRepository.GetAllUser();
            var userReaction = _feedbackRepository.GetAllReaction();
            foreach (var post in posts)
            {
                var appPost = new AppPost();
                appPost.Id = post.Id;
                appPost.Text = post.Text;
                appPost.UserId = post.UserId;
                appPost.CreatedOn = post.CreatedOn;

                var user = users.FirstOrDefault(u => u.Id == post.UserId);
                appPost.UserName = user == null ? "" : user.UserName;
                var commentOfThisPost = comments.Where(c => c.PostId == post.Id);
                var commentList = new List<AppComment>();
                foreach (var comment in commentOfThisPost)
                {
                    commentList.Add(new AppComment
                    {
                        Id = comment.Id,
                        CreatedOn = comment.CreatedOn,
                        PostId = comment.PostId,
                        Text = comment.Text,
                        TotalDislike = userReaction.Where(r => r.UserId == user.Id && r.CommentId == comment.Id && !r.IsLike).Count(),
                        TotalLike = userReaction.Where(r => r.UserId == user.Id && r.CommentId == comment.Id && r.IsLike).Count(),
                        UserId = comment.UserId,
                        UserName = user == null ? "" : user.UserName
                    });                   

                }
                appPost.Comments = commentList;
                appPost.TotalComment = commentList.Count;
                postList.Add(appPost);
            }
            return postList;
        }
    }
}
