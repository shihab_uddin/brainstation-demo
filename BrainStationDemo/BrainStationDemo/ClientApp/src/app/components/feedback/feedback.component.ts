import { OnInit, Component } from "@angular/core";
import { FeedbackService } from "../../services/feedback.service";
import { Post } from "../../models/post.model";
import { Comment } from "../../models/comment.model";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  postList: Array<Post> = [];
  comments: Array<Comment> = [];
  ngOnInit(): void {    
    this.initValues();
  }
  constructor(public feedbackService: FeedbackService) {
  }
  getFeedBack() {
    this.feedbackService.getFeedback().subscribe((result: any) => {
      this.postList.push(result);
    })
  }
  initValues() {
    this.comments.push({ Id: 1, PostId: 1, CreatedOn:new Date(), Text: 'Comment 1', TotalLike: 500, TotalDislike: 50, UserId:1, UserName: 'User 1' });
    this.postList.push({ Id: 1, Comments: this.comments, CreatedOn: new Date(), Text: 'Post 1', TotalComment: 5, UserId: 1, UserName: 'User 1' })
    this.getFeedBack();
  }

}
