export class Comment {
  Id: number;
  Text: string;
  CreatedOn: Date;
  PostId: number
  UserId: number;
  UserName: string;
  TotalLike: number;
  TotalDislike: number;
}
