import { Comment } from './comment.model';
export class Post {
  Id: number;
  Text: string;
  CreatedOn: Date;
  UserId: number;
  UserName: string;
  TotalComment: number;
  Comments: Array<Comment>;
}
