Create database BrainStationDemo
Create table [User](
Id bigint identity(1,1) primary key,
UserName nvarchar(50) not null UNIQUE,
UserRole int not null
)
Create table Post(
Id bigint identity(1,1) primary key,
Text nvarchar(max) not null,
CreatedOn DateTime not null,
UserId bigint not null foreign key REFERENCES [User](Id)
)

Create table Comment(
Id bigint identity(1,1) primary key,
Text nvarchar(max) not null,
CreatedOn DateTime not null,
PostId bigint not null foreign key REFERENCES Post(Id),
UserId bigint not null foreign key REFERENCES [User](Id)
)
Create table ReactionOnComment(
Id bigint identity(1,1) primary key,
IsLike bit not null,
UserId bigint not null foreign key REFERENCES [User](Id),
CommentId bigint not null foreign key REFERENCES Comment(Id)
)